<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Project;
use App\Models\Category;
use App\Http\Requests\StoreProjectRequest;

class ProjectController extends Controller
{
    public function index()
    {
        return view('projects.index', [
            'projects' => Project::all(),
        ]);
    }

    public function create()
    {
        return view('projects.create', [
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]);
    }

    public function store(StoreProjectRequest $request)
    {
        $validatedData = $request->validated();

        try {
            // Project is free software by default
            $validatedData['is_free_software'] = true;
            $project = Project::create($validatedData);
            // Generate a token to edit project
            $project->createAdminToken();

            $project->tags()->sync($validatedData['tags']);

            return redirect()->route('projects.success', $project)->with([
                'authorization' => 'success'
            ]);

        } catch (\Exception $e) {
            throw new \Exception("Impossible de créer le projet : " . $e->getMessage(), 500);
        }
    }

    public function show(Project $project)
    {
        return view('projects.show', [
            'project' => $project,
        ]);
    }

    public function edit(Project $project, string $token)
    {
        return view('projects.edit', [
            'project' => $project,
            'tags' => Tag::all(),
            'categories' => Category::all()
        ]);
    }

    public function update(StoreProjectRequest $request, Project $project, string $token)
    {
        $validatedData = $request->validated();

        try {
            $project->update($validatedData);

            $project->tags()->sync($validatedData['tags']);

            return $project;
        } catch (\Exception $e) {
            throw new \Exception("Impossible de modifier le projet " . $e->getMessage(), 500);
        }
    }

    /**
     * Route after project was stored
     */
    public function success(Project $project)
    {
        abort_if(!session()->has('authorization'), 403);

        return view('projects.success', [
            'link' => $project->path_admin,
        ]);
    }

    public function destroy(Project $project, string $token)
    {
        try {
            $project->delete();

        } catch (\Exception $e) {
            throw new \Exception("Impossible de supprimer le projet " . $e->getMessage(), 500);
        }
    }
}
