<?php

namespace App\Policies;

use App\Models\Project;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    public function manage(?User $user = null, Project $project, string $token)
    {
        return $project->admin_token === $token;
    }
}
