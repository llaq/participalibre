<?php

namespace App\Models;

use App\Models\Project;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
