<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') ⋅ Participalibre</title>
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div id="big_container" class="participalibre">
                <header class="big-header">
                    <div class="columns">
                        <div class="column">
                            <div class="home-link">
                                <a href="/">
                                    <h1>
                                        Contribution libre
                                    </h1>
                                    <sub>
                                        Votre expérience est la bienvenue&nbsp;!
                                    </sub>
                                </a>
                            </div>
                        </div>
                        <div class="column text-right">
                            @if (Route::has('login'))
                            <div class="top-right links">
                                @auth
                                <a href="{{ url('/home') }}">Home</a>
                                @else
                                <a href="{{ route('login') }}">Login</a>

                                @if (Route::has('register'))
                                <a href="{{ route('register') }}">Register</a>
                                @endif
                                @endauth
                            </div>
                            @endif

                            <nav class="links text-normal">
                                <a href="/about">
                                    À propos
                                </a>
                                <a href="#">
                                    Contact
                                </a>
                            </nav>
                        </div>
                    </div>
                </header>
                <main>
                    <header>
                        @yield('content-header')
                    </header>
                    <section id="content">
                        @yield('content')
                    </section>
                </main>
                <footer>
                    <div class="links">
                        À propos du projet :
                        <a href="/">Code source</a>
                        <a href="/">Feedback</a>
                        <a href="/">Documentation</a>
                    </div>
                </footer>
            </div>
        </div>
    </div>
    <script defer="true" src="/js/app.js"></script>
</body>

</html>