@extends('layouts.app')

@section('title', 'Ajouter un projet')

@section('content')
    <section class="add-form">
        <h1>Ajouter un projet</h1>
        <h2>
            Une description simple et précise de votre projet et de votre besoin permettra une meilleure compréhension
            auprès des futur·es contributeur·rices.
            Une demande de contribution correspond à un besoin.
            Remplissez autant de demandes que vous avez de besoins !
        </h2>
        <form method="post" action="/projects" class="child-margin-vertical-medium">
            @csrf
            <div>
                <label for="name" class="label">Le nom de votre projet</label>
                <label for="name" class="sublabel">Afin d’identifier rapidement votre projet</label>
                <input placeholder="Participalibre" type="text" name="name" id="name" required>
            </div>
            <div>
                <label for="description" class="label">Sa présentation</label>
                <label for="description" class="sublabel">Description du service proposé</label>
                <textarea placeholder="Facilitateur d’échanges entre porteur·ses de projets et contributeur·rices en ligne"
                    name="description" id="description" cols="30" rows="10" required></textarea>
            </div>
            <div>
                <label for="url" class="label">Son lien</label>
                <label for="url" class="sublabel">Son adresse URL, si c'est possible</label>
                <input placeholder="Lien" type="url" name="url" id="url" required>
            </div>
            <div>
                <label for="licence" class="label">Sa licence</label>
                <label for="licence" class="sublabel">Son nom ou lien si c’est possible </label>
                <input placeholder="AGPL v3, Creative Commons..." type="text" name="licence" id="licence" required>
            </div>
            <div>
                <p class="label">Le type de contribution souhaitée</p>
                <p class="sublabel">Choisissez ce dont vous avez besoin dans une seule catégorie (2 compétences maximum)</p>
                <div class="child-margin-vertical-small">
                    <div class="choose accordion">
                        <div class="toggle-element columns flex-space-between">
                            <div class="column">
                                <p class="label">
                                    Développement
                                </p>
                                <p class="sublabel">
                                    Programmer dans un ou des langages spécifiques
                                </p>
                            </div>
                            <i class="chevron-up"></i>
                        </div>
                        <div class="content">
                            <i class="delimiter"></i>
                            <div class="buttons columns flex-left" style="margin: 10px;">
                                @foreach ($tags as $tag)
                                    <input type="checkbox" name='tags[]' id="tag_{{ $tag->id }}" class="cb" value="{{ $tag->id }}">
                                    <label for="tag_{{ $tag->id }}">
                                        {{ $tag->name }}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="choose accordion">
                        <div class="toggle-element columns flex-space-between">
                            <div class="column">
                                <p class="label">
                                    Développement
                                </p>
                                <p class="sublabel">
                                    Programmer dans un ou des langages spécifiques
                                </p>
                            </div>
                            <i class="chevron-up"></i>
                        </div>
                        <div class="content">
                            <i class="delimiter"></i>
                            <div class="buttons columns flex-left" style="margin: 10px;">
                                <input type="checkbox" name="tags[4]" id="html" class="cb">
                                <label for="html">
                                    HTML/CSS
                                </label>
                                <input type="checkbox" name="tags[5]" id="js" class="cb">
                                <label for="js">
                                    Javascript
                                </label>
                                <input type="checkbox" name="tags[6]" id="sql" class="cb">
                                <label for="sql">
                                    SQL
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <label for="details" class="label">Détails de la contribution</label>
                <label for="details" class="sublabel">Précisez le besoin</label>
                <textarea placeholder="Je ne peux pas traduire mes textes" name="details" id="details" cols="30" rows="10"
                    required></textarea>
            </div>
            <div>
                <label for="remuneration" class="label">Rémunération</label>
                <label for="remuneration" class="sublabel">Précisez si la contribution sera rémunérée</label>
                <input placeholder="Financière ou autre" type="text" name="remuneration" id="remuneration" required>
            </div>
            <div>
                <label for="creator" class="label">Votre prénom / pseudo / structure</label>
                <label for="creator" class="sublabel">Pour vous reconnaître</label>
                <input placeholder="Zoé (Agence Kitrouvtout)" type="text" name="creator" id="creator" required>
            </div>
            <div>
                <label for="email" class="label">Votre adresse mail</label>
                <label for="email" class="sublabel">Pour vous contacter. Cette info ne sera pas publique</label>
                <input placeholder="contact@participalibre.fr" type="email" name="email" id="email" required>
            </div>
            <div>
                <input type="checkbox" name="accept" class="checkbox" id="accept" required>
                <label for="accept" class="label">J'accepte que ces informations soient publiées sur la plateforme</label>
                <label for="accept" class="sublabel">Vous pourrez modifier ou retirer ce formulaire</label>
            </div>
            <div class="btns">
                <a href="javascript:history.back()" style="margin-right: 40px;" class="link">Retour</a>
                <button type="submit" class="button-primary arrow">Valider</button>
            </div>
        </form>
    </section>
@endsection
