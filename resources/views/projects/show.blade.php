@extends('layouts.app')

@section('title', 'Nom du projet')

@section('content')
<section id="show">
  <header>
    <span class="category text-large">Aide facile et rapide</span>
    <span class="tag text-large">Tester un projet</span>
    <p>Publiée le 20/11/2020 à 19h15</p>
    <h1>Contribulle</h1>
  </header>
  <section class="child-margin-vertical-medium width-1-2">
    <div>
      <h2 class="text-normal margin-none">Description</h2>
      <p class="margin-none">Participalibre se veut être le point de rencontre entre des porteur·ses de projets libres
        et des
        contributeur·rices. L’idée est de montrer que les contributions sont accessibles et variées. Tout le monde peut
        participer au bien commun !</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Lien</h2>
      <a href="#" class="text-normal text-primary">https://contribulle.org</a>
    </div>

    <div>
      <h2 class="text-normal margin-none">Licence</h2>
      <p class="margin-none text-normal text-secondary">Licence MIT</p>
    </div>

    <div>
      <h2 class="text-normal">Le type de contribution souhaité</h2>
      <div>
        <span class="tag text-large">Tester un projet</span>
      </div>
      <p>Il faudrait tester la fonctionnalité « J’ai un projet » et nous faire un retour sur le parcours</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Rémunération</h2>
      <p class="margin-none">Nous souhaitons rester une association bénévole.</p>
    </div>

    <div>
      <h2 class="text-normal margin-none">Contact</h2>
      <p class="margin-none">Équipe Contribulle : <a class="text-primary" href="">contact@contribulle.org</a></p>
    </div>
  </section>
  <div class="margin-xlarge-top columns flex-items-center">
    <div class="width-1-2">
        <button class="button-danger">Signaler l’annonce</button>
    </div>
    <div class="width-1-2">
        <a href="/" class="link">Retour à l’accueil</a>
    </div>
</div>
</section>
@endsection
