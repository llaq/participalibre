@extends('layouts.app')

@section('title', "Les projets qui ont besoin d’aide")

@section('content-header')
<div class="padding-medium background-accent overflow-hidden">
    <h1>C’est votre première contribution et vous êtes perdu⋅e⋅s ?
        <br>
        On vous a séléctioné quelques projets
    </h1>
    <div id="slider">
        <div class="slider-content">
            <div class="card-header">
                <h2 class="text-normal">Common Voice</h2>
            </div>
            <div class="card-body">
                <p>
                    Écouter ou enregistrer sa voix.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </a>
        </div>
        <div class="slider-content">
            <div class="card-header">
                <h2 class="text-normal">StreetComplete</h2>
            </div>
            <div class="card-body">
                <p>
                    Compléter des informations de lieux près de chez vous.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </a>
        </div>
        <div class="slider-content">
            <div class="card-header">
                <h2 class="text-normal">Cuisine Libre</h2>
            </div>
            <div class="card-body">
                <p>
                    Créer des fiches de recettes.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </a>
        </div>
        <div class="slider-content">
            <div class="card-header">
                <h2 class="text-normal">Wikimédia</h2>
            </div>
            <div class="card-body">
                <p>
                    Enrichir une médiathèque avec vos images ou dessins.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </a>
        </div>
        <div class="slider-content">
            <div class="card-header">
                <h2 class="text-normal">Wikipédia</h2>
            </div>
            <div class="card-body">
                <p>
                    Relire ou reformuler des bouts d’article encyclopédiques.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </a>
        </div>
        <div class="slider-content">
            <div class="card-header">
                <h2 class="text-normal">Common Voice</h2>
            </div>
            <div class="card-body">
                <p>
                    Écouter ou enregistrer sa voix
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir le site</span>
            </a>
        </div>

    </div>
</div>
@endsection

@section('content')
<section>
    <h1>Les projets qui ont besoin d’un coup de main</h1>
    <div class="child-margin-vertical-medium">
        <article class="card card-radius">
            <div class="card-header columns">
                <span class="category">Texte</span>
                <span class="tag">Relecture</span>
            </div>
            <hr>
            <div class="card-body">
                <h2 class="text-normal">Meetic du libre</h2>
                <p>
                    Facilitateur d’échanges entre porteur·ses de projets et contributeur·rices en ligne.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir l’annonce</span>
            </a>
        </article>
        <article class="card card-radius">
            <div class="card-header columns">
                <span class="category">Texte</span>
                <span class="tag">Relecture</span>
            </div>
            <hr>
            <div class="card-body">
                <h2 class="text-normal">Meetic du libre</h2>
                <p>
                    Facilitateur d’échanges entre porteur·ses de projets et contributeur·rices en ligne.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir l’annonce</span>
            </a>
        </article>
        <article class="card card-radius">
            <div class="card-header columns">
                <span class="category">Texte</span>
                <span class="tag">Relecture</span>
            </div>
            <hr>
            <div class="card-body">
                <h2 class="text-normal">Meetic du libre</h2>
                <p>
                    Facilitateur d’échanges entre porteur·ses de projets et contributeur·rices en ligne.
                </p>
            </div>
            <a href="#" style="display: block" class="inverse card-footer text-right">
                <i class="arrow-right"></i>
                <span>Voir l’annonce</span>
            </a>
        </article>
    </div>
</section>
@endsection