@extends('layouts.app')

@section('title', 'Bienvenue')

@section('content')
    <section class="home">
        <div class="columns">
            <div class="column">
                <a href="{{ route('projects.create') }}" class="tile">
                    <h2>J'ai un projet</h2>
                    <span class="desc">
                        Et j'aimerais avoir un coup de main
                    </span>
                </a>
            </div>
            <div class="column">
                <a href="{{ route('projects.index') }}" class="tile">
                    <h2>Je veux contribuer</h2>
                    <span class="desc">
                        J'ai envie de participer à l'évolution d'un outil
                    </span>
                </a>
            </div>
        </div>
    </section>
@endsection
