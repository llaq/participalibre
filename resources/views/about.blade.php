@extends('layouts.app')

@section('title', 'À propos')

@section('content')
<section class="text-secondary text-justify text-large" id="about">
  <article class="width-3-4">
    <h1>Qu’est-ce que Contribulle ?</h1>
    <h2 class="text-normal">« Je veux contribuer au logiciel libre mais je ne sais pas comment et où »</h2>
    <p>
      Contribulle est une réponse à cette requête à laquelle nous sommes souvent confronté·es !
      Cette plateforme - à licence - libre permet de simplifier le contact entre les projets - partageant les valeurs du
      logiciel libre et des communs - qui manquent de compétences, et de super·be·s contributeur·rice·s qui pourraient
      leur donner un coup de main.
    </p>
    <p>
      Le logiciel libre et les communs, c’est une affaire qui concerne tout le monde. Pour une émancipation à la fois
      individuelle et collective, la liberté de tout·e·s, la coopération et le partage de connaissances sont des
      principes
      plus qu’essentiels.
      Contribulle s’inscrit d’ailleurs dans un mouvement qui met en avant la contribution par et pour tout le monde,
      pour
      réellement participer et construire une société inclusive. Chacun·e peut aider à sa manière !

    </p>

    <h2 class="text-normal">Derrière Contribulle : une équipe motivée et une démarche progressive</h2>
    <div>
      <p>
        Contribulle est créée de façon itérative, avec une équipe composée de personnes aux compétences variées ! Pour
        papoter avec nous, vous pouvez :
      </p>
      <ul>
        <li>Nous rejoindre sur Framateam ;</li>
        <li>Nous écrire par mail sur bonjour@contribulle.org</li>
      </ul>
      <p>
        Nous avons hâte de contribuer au libre et aux communs avec vous !
      </p>
    </div>
  </article>
  <div class="text-center padding-large">
    <a href="/" class="link">Retour à l’accueil</a>
  </div>
</section>
@endsection
