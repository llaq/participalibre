<?php

use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */

Route::view('/', 'welcome')->name('home');
Route::view('/demo', 'demo');
Route::view('/projects-list', 'projects-list');
Route::view('/about', 'about');
Route::resource('projects', ProjectController::class)->except(['edit', 'update', 'destroy', 'success']);
Route::get('/projects/success', [ProjectController::class, 'success'])->name('projects.success');
Route::get('/projects/{project}/success', [ProjectController::class, 'success'])->name('projects.success');

Route::group(['middleware' => ['can:manage,project,token']], function () {
    Route::get('/projects/{project}/edit/{token}', [ProjectController::class, 'edit'])->name('projects.edit');
    Route::patch('/projects/{project}/{token}', [ProjectController::class, 'update'])->name('projects.update');
    Route::delete('/projects/{project}/{token}', [ProjectController::class, 'destroy'])->name('projects.destroy');
});
