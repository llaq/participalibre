# Contribulle

![Logo](https://framagit.org/uploads/-/system/project/avatar/63595/propositionlogo.png?width=64)

Bienvenue sur le dépôt dédié au développement de **Contribulle** (Anciennement Participalibre).

Le but de cet outil est :  

 * d'aider les personnes en charge de projets libres à trouver des volontaires pour contribuer à leur projet ;
 * d'aider les volontaires à trouver des projets sur lesquels ils peuvent contribuer en fonction de leurs compétences et centres d'intérêts ;
 * de donner de la visibilité aux projets libres nécessitant des contributions.

## Contribuer à ce projet

Que vous soyez débutant·e, confirmé·e ou expert·e, toute aide est la bienvenue !
Vous pouvez par exemple :

- contribuer à l'amélioration du [wiki](https://framagit.org/participalibre/participalibre/-/wikis/home) ;
- contribuer par des  tests à l'amélioration de l'ergonomie ;
- contribuer aux graphismes (logo, icones,...) ;
- aider à relire l'interface et la documentation ;
- communiquer autour du projet ;
- proposer des améliorations en créant une [issue](https://framagit.org/participalibre/participalibre/-/issues) ;
- contribuer au code ;
- participer à la  [Framateam](https://framateam.org/signup_user_complete/?id=8oau5fya67r19xz93fcez547ar).

## Licence

Ce projet est développé sous la licence [AGPLv3+](https://www.gnu.org/licenses/agpl-3.0.html), ce qui en fait un **logiciel Libre**.
