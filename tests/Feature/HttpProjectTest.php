<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Project;
use App\Models\Tag;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Illuminate\Testing\TestResponse;
use Tests\TestCase;

class HttpProjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_project_can_be_added()
    {
        $this->createTag();

        $response = $this->createProject([
            'name' => 'New Project',
            'description' => 'Project description',
            'url' => 'https://myprojects.org',
            'email' => 'john@example.com',
            'creator' => 'John',
            'details' => 'Beautiful Project',
            'licence' => 'AGPLv3+',
            'remuneration' => 'Free',
            'is_free_software' => 1,
            'tags' => [
                1,
            ],
        ]);

        $this->assertCount(1, Project::all());
    }

    /** @test */
    public function success_view_can_be_seen_only_with_specific_session_data()
    {
        $project = Project::factory()->withAdminToken()->create();

        $response = $this->get(route('projects.success', $project));
        $response->assertStatus(403);

        $response = $this->withSession(['authorization' => 'success'])->get(route('projects.success', $project));
        $response->assertStatus(200);
    }
    

    /** @test */
    public function a_project_has_an_admin_token_generated()
    {
        $this->createTag();
        $this->createProject($this->data());
        $project = Project::first();

        $this->assertNotNull($project->admin_token);
    }

    /** @test */
    public function a_project_has_at_least_one_tag_after_its_creation()
    {
        $this->createTag();
        $this->createProject($this->data());
        $project = Project::first();

        $this->assertCount(1, $project->tags);
    }

    /** @test */
    public function a_project_can_be_edited_with_correct_token()
    {
        $this->createTag();
        $this->createProject($this->data());
        $project = Project::first();

        $response = $this->patch(route('projects.update', [$project, $project->admin_token]), array_merge($this->data(), [
            'name' => 'Updated Project',
        ]));

        $response->assertStatus(200);

        $this->assertEquals('Updated Project', $project->fresh()->name);
    }

    /** @test */
    public function a_project_cannot_be_edited_with_incorrect_token()
    {
        $this->createTag();
        $this->createProject($this->data());
        $project = Project::first();

        $response = $this->patch(route('projects.update', [$project, 'incorrect-token']), array_merge($this->data(), [
            'name' => 'Updated Project',
        ]));

        $response->assertStatus(403);

        $this->assertEquals('New Project', $project->fresh()->name);
    }

    /** @test */
    public function a_project_can_be_deleted_with_correct_token()
    {
        $this->createTag();
        $this->createProject($this->data());
        $project = Project::first();
        
        $this->assertCount(1, Project::all());

        $response = $this->delete(route('projects.destroy', [$project, $project->admin_token]));

        $response->assertStatus(200);

        $this->assertCount(0, Project::all());
    }

    /** @test */
    public function a_project_cannot_be_deleted_with_incorrect_token()
    {
        $this->createTag();
        $this->createProject($this->data());
        $project = Project::first();
        
        $this->assertCount(1, Project::all());

        $response = $this->delete(route('projects.destroy', [$project, 'incorrect-token']));

        $response->assertStatus(403);

        $this->assertCount(1, Project::all());
    }

    private function data(): array
    {
        return [
            'name' => 'New Project',
            'description' => 'Project description',
            'url' => 'https://myprojects.org',
            'email' => 'john@example.com',
            'creator' => 'John',
            'details' => 'Beautiful Project',
            'licence' => 'AGPLv3+',
            'remuneration' => 'Free',
            'is_free_software' => 1,
            'tags' => [
                0 => 1,
            ],
        ];
    }

    private function createProject(array $data = []): TestResponse
    {
        return $this->followingRedirects()->post(route('projects.store'), $data);
    }

    private function createTag(string $name = 'Tag'): void
    {
        $category = Category::create([
            'name' => 'category',
            'slug' => 'category',
        ]);

        $category->tags()->create([
            'name' => $name,
            'slug' => Str::slug($name),
        ]);
    }
}
